package ru.aar.area;

public abstract class Figure {
    private Color color;

    public Figure(){
        this(Color.BLACK);
    }

    public Figure(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public abstract double area();

}
