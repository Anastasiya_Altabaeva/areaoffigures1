package ru.aar.area;

public class Circle extends Figure {
    private Point center;
    private double radius;

    Circle(Color color, Point center, double radius){
        super(color);
        this.center = center;
        this.radius = radius;
    }
    public double area() {
        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return "Круг{" +
                "цвет = " + getColor() +
                ", центр= " + center +
                ", радиус= " + radius +
                "}";
    }
}
