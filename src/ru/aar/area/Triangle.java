package ru.aar.area;

public class  Triangle extends Figure {
    private Point a;
    private Point b;
    private Point c;

    Triangle(Color color, Point a, Point b, Point c){
        super(color);
        this.a= a;
        this.b = b;
    }
    public double area() {

        return Math.abs(a.getX() - c.getX()) * (b.getY() - c.getY()) - (b.getX() - c.getX()) * (a.getY() - c.getY()) / 2;
    }

    @Override
    public String toString() {
        return "Треугольник{" +
                "цвет = " + getColor() +
                ", сторона а = " + a +
                ", сторона b = " + b +
                ", сторона с = " + c +
                "}";
    }
}