package ru.aar.area;

public class Point {
    private double x;
    private double y;

    Point( double x, double y){
        this.x = x;
        this.y = y;
    }
    Point(){this(0,0);}

    @Override
    public String toString() {
        return "Point{" +
                "x= " + x +
                ",y= " + y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
