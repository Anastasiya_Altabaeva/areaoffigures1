package ru.aar.area;

public class Demo {
    public static void main(String[] args) {
        Circle circle = new Circle(Color.BLACK, new Point(), 5);
        Triangle triangle = new Triangle(Color.PINK, new Point(0, 0), new Point(1, 0), new Point(0, 1));
        Square square = new Square(Color.RED, new Point(1, 1), 5);

        Figure[] figures = {circle, triangle, square};
        printArrayElements(figures);

        Figure maxFigure = maxFigureArea(figures);
        System.out.println("Фигура с наибольшоей площадью " + maxFigure);
    }


        private static void printArrayElements(Object[] objects) {
            for (Object object : objects) {
                System.out.println(object);
            }
        }

        private static Figure maxFigureArea(Figure[] figures) {
            Figure maxFigure = null;
            double maxArea = Double.NEGATIVE_INFINITY;
            for (Figure figure : figures) {
                double area = figure.area();
                if (area > maxArea) {
                    maxArea = area;
                    maxFigure = figure;
                }
            }
            return maxFigure;
        }
    }

