package ru.aar.area;

public class  Square extends Figure {
    private Point corner;
    private double side;

    Square(Color color, Point center, double radius){
        super(color);
        this.corner = corner;
        this.side = side;
    }
    public double area() {
        return side * side;
    }

    @Override
    public String toString() {
        return "Квадрат{" +
                "цвет = " + getColor() +
                ", угол = " + corner +
                ", сторона = " + side +
                "}";
    }
}


